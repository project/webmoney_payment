/**
 * @file
 * Automatically submit the payment redirect form.
 */

(function ($) {
  Drupal.behaviors.webmoneyPayment = {
    attach: function (context, settings) {
      $('div.webmoney-redirect-form form', context).submit();
    }
  };
})(jQuery);
