This module provide integration http://webmoney.ru payments for
https://www.drupal.org/project/payment (7.x-1.x version)

Registering with webmoney
Before you start the installation process you must register on webmoney.ru
and create your own Merchant. You will get "Merchant key", "Password" and
other settings for your payment system.

Installation and Configuration
Download the module from Drupal.org and extract it to your modules folder.
Enable it.
Go to /admin/config/services/payment/method/add and add Webmoney payment method.
Setup the settings according your data from webmoney.

Additional information:

Default result URL: http://yoursitename.com/webmoney/result
Default Success URL: http://yoursitename.com/webmoney/success
Default Fail URL: http://yoursitename.com/webmoney/fail

More API documentation can be found at Webmoney API
