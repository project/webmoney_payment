<?php
/**
 * @file
 * Contains WebmoneyPaymentController.
 */

/**
 * A Webmoney payment method controller.
 */
class WebmoneyPaymentController extends PaymentMethodController {

  /**
   * Webmoney server payment URL.
   */
  const SERVER_URL = 'https://merchant.webmoney.ru/lmi/payment.asp';

  /**
   * Default controller data.
   *
   * @var array
   */
  public $controller_data_defaults = array(
    'lmi_payee_purse' => '',
    'secret_key_x20' => '',
    'secret_key' => '',
    'test_mode' => TRUE,
    'lmi_sim_mode' => '0',
  );

  /**
   * Payment method configuration form elements callback.
   *
   * @var string
   */
  public $payment_method_configuration_form_elements_callback = 'webmoney_payment_payment_method_configuration_form_elements';

  /**
   * Payment configuration form elements callback.
   *
   * @var string
   */
  public $webmoney_configuration_form_elements_callback = 'webmoney_payment_payment_configuration_form_elements';

  /**
   * WebmoneyPaymentController constructor.
   */
  public function __construct() {
    $this->title = t('Webmoney');
    $this->description = t('Webmoney payment method.');
  }

  /**
   * Implements PaymentMethodController::execute().
   */
  public function execute(Payment $payment) {
    entity_save('payment', $payment);
    drupal_goto('webmoney/redirect/' . $payment->pid);
  }

  /**
   * Payment form generator.
   *
   * @param \Payment $payment
   *   Payment object.
   *
   * @return array
   *   Payment form array.
   */
  static public function getPaymentForm(Payment $payment) {
    $form = array();
    $desc = iconv("utf-8", "windows-1251", $payment->description);
    $form_data = array(
      'LMI_PAYEE_PURSE' => $payment->method->controller_data['lmi_payee_purse'],
      'LMI_PAYMENT_NO' => $payment->pid,
      'LMI_PAYMENT_AMOUNT' => $payment->totalAmount(TRUE),
      'LMI_PAYMENT_DESC' => $desc,
      'LMI_PAYMENT_DESC_BASE64' => base64_encode($desc),
      // @todo LMI_PAYMENTFORM_SIGN block payment process with err: 8 step:131, need clerification with WM support.
      // 'LMI_PAYMENTFORM_SIGN' => self::signature($payment),
    );
    if ($payment->method->controller_data['test_mode']) {
      $form_data['LMI_SIM_MODE'] = $payment->method->controller_data['lmi_sim_mode'];
    }

    $form['#action'] = self::SERVER_URL;
    $form['#attributes'] = array(
      'name' => 'payment',
      'accept-charset' => 'windows-1251',
    );
    foreach ($form_data as $key => $value) {
      $form[$key] = array(
        '#type' => 'hidden',
        '#value' => $value,
      );
    }
    return $form;
  }

  /**
   * Post data validator.
   *
   * @param array $data
   *   Post data.
   *
   * @return bool
   *   TRUE if post data is valid FALSE otherwise.
   */
  static public function validatePost(array $data) {
    if (empty($data)) {
      return FALSE;
    }
    if (!isset($data['LMI_PAYMENT_NO'])) {
      return FALSE;
    }
    $payment = entity_load_single('payment', $data['LMI_PAYMENT_NO']);
    if (!$payment) {
      return FALSE;
    }
    if ($payment->totalAmount(TRUE) != $data['LMI_PAYMENT_AMOUNT']) {
      return FALSE;
    }
    if ($payment->method->controller_data['lmi_payee_purse'] != $data['LMI_PAYEE_PURSE']) {
      return FALSE;
    }

    $sing_data = array(
      $data['LMI_PAYEE_PURSE'],
      $data['LMI_PAYMENT_AMOUNT'],
      $data['LMI_PAYMENT_NO'],
      $data['LMI_MODE'],
      $data['LMI_SYS_INVS_NO'],
      $data['LMI_SYS_TRANS_NO'],
      $data['LMI_SYS_TRANS_DATE'],
      $payment->method->controller_data['secret_key'],
      $data['LMI_PAYER_PURSE'],
      $data['LMI_PAYER_WM'],
    );
    $signature_string = implode('', $sing_data);
    $hash = hash('SHA256', $signature_string);
    $sign = drupal_strtoupper($hash);

    if ($data['LMI_HASH'] != $sign) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Generate payment signature.
   *
   * @param \Payment $payment
   *   Payment object.
   *
   * @return string
   *   Signature.
   */
  static public function signature(Payment $payment) {
    $signature_data = array(
      $payment->method->controller_data['lmi_payee_purse'],
      $payment->totalAmount(TRUE),
      $payment->pid,
      $payment->method->controller_data['secret_key'],
    );
    $signature_string = implode('', $signature_data);
    $hash = hash('SHA256', $signature_string);
    return drupal_strtoupper($hash);
  }

}
